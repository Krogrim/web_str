<?php

class Route
{
    static function start()
    {
        // контроллер и действие по умолчанию
        $controllerName = 'main';
        $actionName = 'index';

        $routes = explode('/', $_SERVER['REQUEST_URI']);

        // наше действие
        if (isset($routes[1]) && (mb_strlen($routes[1]) > 0)) {
            $actionName = $routes[1];
        }

        // добавляем префиксы
        $modelName = 'model_' . $controllerName;
        $controllerName = 'controller_' . $controllerName;
        $actionName = 'action_' . $actionName;

        // файл модели, если есть
        $modelFile = strtolower($modelName) . '.php';
        $model_path = "app/models/" . $modelFile;
        if (file_exists($model_path)) {
            include "app/models/" . $modelFile;
        }

        // цепляем файл с классом контроллера
        $controllerFile = strtolower($controllerName) . '.php';
        $controller_path = "app/controllers/" . $controllerFile;
        if (file_exists($controller_path)) {
            include "app/controllers/" . $controllerFile;
        } else {
            self::ErrorPage404();
        }

        // создаем контроллер
        $controller = new $controllerName;
        $action = $actionName;
        if (method_exists($controller, $action)) {

            $controller->$action();
        } else {

            self::ErrorPage404();
        }

    }

    protected static function ErrorPage404()
    {
        $host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:' . $host . '404');
    }
}