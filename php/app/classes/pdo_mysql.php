<?php

class Db
{
    // Переменная, хранящая объект PDO
    private PDO $db;

    public function __construct()
    {
        // Подключение
        try {
            $this->db = new PDO('mysql:host=' . DBHOST . ';dbname=' . DBNAME, DBLOGIN, DBPASSWORD,
                array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));
        } catch (PDOException $e) {
            echo json_encode(['Error - ' . $e->getMessage()]);
            die;
        }
    }

    // Операции над БД
    public function query($sql = '', $dataList = []): array|string
    {
        try {

            // Подготовка запроса
            $stmt = $this->db->prepare($sql);

            // параметры запроса
            $paramList = [];
            if (count($dataList) > 0) {

                // item либо параметр, либо массив с параметрами
                // value либо значение параметра, либо массив со значениями
                foreach ($dataList as $item => $value) {

                    // если $dataList массив с несколькими наборами параметр/значение
                    // multiple insert
                    if (is_array($value)) {
                        foreach ($value as $k => $v) {
                            $paramList[] = $v;
                        }
                    } else {

                        // если $dataList список параметров со значениями
                        $paramList[] = $value;
                    }
                }
            }

            // Выполняем запрос
            $stmt->execute($paramList);

            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            return json_encode(["ERROR: Could not able to execute $sql. " . $e->getMessage()]);
        }

    }

    public function doInsert($table, $insertList)
    {
        $keyList = array_keys($insertList);
        $fields = implode(', ', $keyList);

        // count - если $insert_list состоит из нескольких наборов параметров для вставки
        // другими словами multiple insert
        $count = 0;
        if ($keyList[0] === 0) {

            $count = count($insertList);
            $keyList = array_keys($insertList[$keyList[0]]);
            $fields = implode(',', $keyList);
        }

        // подстановка заглушки(?,...) для параметров вставки
        $placeholderList = str_repeat('?,', count($keyList));
        $placeholderList = substr($placeholderList, 0, -1);

        // собираем набор из параметров для вставки VALUE (?,...)
        $valueList = '(' . $placeholderList . ')';

        // если multiple insert - VALUE (?,...),...
        if ($count > 0) {
            $valueList = substr(str_repeat('(' . $placeholderList . '),', $count), 0, -1);
        }

        return $this->query('INSERT INTO ' . $table
            . '(' . $fields . ') VALUES ' . $valueList, $insertList);
    }

    public function doUpdate($table, $primaryKey, $params = [])
    {
        $updateList = '';
        $paramList = [];
        foreach ($params as $k => $v) {
            $updateList .= $k . ' = ?, ';
            $paramList[] = $v;
        }
        $updateList = substr($updateList, 0, -2);

        return $this->query('UPDATE ' . $table . ' SET ' . $updateList . ' WHERE `' . $primaryKey['id'] . '` = ' . $primaryKey['value'], $paramList);
    }

    public function getAll($table, $sql = '', $params = [])
    {
        return $this->query('SELECT * FROM ' . $table . ' ' . $sql, $params);
    }

    public function getOne($table, $primaryKey)
    {
        return $this->query('SELECT * FROM ' . $table . ' WHERE `' . $primaryKey['id'] . '` = ' . $primaryKey['value']);
    }
}