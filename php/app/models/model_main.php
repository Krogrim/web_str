<?php

class Model_Main extends Model
{
    private array $data;

    public function __construct()
    {
        $this->data = self::getParam();
    }

    public function getUserList(): string
    {
        $db = new Db();
        if ($userList = $db->getAll('users')) {
            return json_encode($userList);
        }
        return json_encode(['message' => 'There is no user']);
    }

    public function addUser(): string
    {
        // не нашел способа отправить постманом сразу сырой json и файл для аватара :(
        // в поле формы сделал контент тайп json для строки

        $fileName = explode('/', $_FILES['avatar']['tmp_name'])[2] . '.' .
            explode('.', $_FILES['avatar']['name'])[1];

        $filePath = 'app/user_image/' . $fileName;

        $fileExt = $_FILES['avatar']['type'];
        $validExtensions = ['image/jpeg', 'image/jpg', 'image/png'];
        if (!in_array($fileExt, $validExtensions) or ($_FILES['avatar']['size'] > 1000000)) {
            return json_encode(["Invalid file type or file size too large"]);
        }

        if (!move_uploaded_file($_FILES['avatar']['tmp_name'], $filePath)) {
            return json_encode(['message' => "Oops, file not created!"]);
        }

        $jsonData = json_decode($_POST['json'], true);

        $userInfo =
            [
                'login' => $jsonData['login'],
                'password' => $jsonData['password'],
                'name' => $jsonData['name'],
                'city' => $jsonData['city'],
                'phone' => $jsonData['phone'],
                'avatar' => $filePath,
                'created_at' => time(),
                'updated_at' => time(),
            ];


        $db = new Db();

        if ($db->doInsert('users', $userInfo)) {
            return json_encode(['message' => 'Oops,we have a trouble']);
        }
        return json_encode(['message' => 'New user added, login - ' . $jsonData['login']]);
    }

    // тупо чтобы быстро заполнить базу
    public function addUserFast(): string
    {
        $userInfo = [];
        for ($n = 0; $n < 10; $n++) {

            $userInfo[] = [
                'login' => substr(md5(rand()), 0, 7),
                'password' => substr(md5(rand()), 0, 7),
                'name' => substr(md5(rand()), 0, 7),
                'city' => substr(md5(rand()), 0, 7),
                'phone' => rand(0, 23154645978),
                'avatar' => substr(md5(rand()), 0, 7),
                'created_at' => time(),
                'updated_at' => time(),
            ];
        }

        $db = new Db();

        if ($db->doInsert('users', $userInfo)) {
            return json_encode(['message' => 'Oops,we have a trouble']);
        }
        return json_encode(['message' => 'Added 10 new users']);
    }

    public function getCommentList(): string
    {
        $db = new Db();

        if ($commentList = $db->getAll('comments')) {
            return json_encode($commentList);
        }
        return json_encode(['message' => 'There is no comments']);
    }

    public function addComment(): string
    {
        $userId =
            [
                'id' => 'user_id',
                'value' => $this->data['user_id']
            ];

        $db = new Db();
        $user = $db->getOne('users', $userId);

        if (count($user) == 0) {
            return json_encode(['message' => "User not found"]);
        }

        $commentInfo =
            [
                'user_id' => $this->data['user_id'],
                'message' => $this->data['message'],
                'created_at' => time(),
                'updated_at' => time(),
            ];

        if ($db->doInsert('comments', $commentInfo)) {
            return json_encode(['message' => 'Oops,we have a trouble']);
        }
        return json_encode(['message' => 'New comment added']);
    }

    // быстрые сообщения
    public function addCommentFast(): string
    {
        $db = new DB();
        $userList = $db->getAll('users');

        if (count($userList) == 0) {
            return json_encode(['message' => 'Add new user pls']);
        }

        $userIdLIst = [];
        foreach ($userList as $k => $v) {
            $userIdLIst[] = $v['user_id'];
        }

        $commentList = [];
        for ($n = 0; $n < 10; $n++) {

            $commentList[] =
                [
                    'user_id' => array_rand($userIdLIst, 1),
                    'message' => substr(md5(rand()), 0, 7),
                    'created_at' => time(),
                    'updated_at' => time(),
                ];
        }

        if ($db->doInsert('comments', $commentList)) {
            return json_encode(['message' => 'Oops,we have a trouble']);
        }
        return json_encode(['message' => 'Added 10 new comments']);
    }

    public function doCommentStatusChange(): string
    {
        $commentId =
            [
                'id' => 'comment_id',
                'value' => $this->data['comment_id'],
            ];

        $db = new Db();
        if(!$db->getOne('comments', $commentId)){
            return json_encode(['message' => 'Comment with id: ' . $this->data['comment_id'] .' not exist']);
        }

        $statusUpdate =
            [
                'status' => $this->data['status'],
            ];

        $db = new Db();

        if ($db->doUpdate('comments', $commentId, $statusUpdate)) {
            return json_encode(['message' => 'Oops,we have a trouble']);
        }
        return json_encode(['message' => 'Comment: ' . $this->data['comment_id'] . ' status changed: ' . $this->data['status']]);
    }

    public function doDeleteComments(): string
    {
        $db = new Db();

        $commentId =
            [
                'id' => 'comment_id',
                'value' => $this->data['comment_id'],
            ];

        $comment = $db->getOne('comments', $commentId);

        if (count($comment) == 0) {
            return json_encode(['message' => "Comment with id: " . $this->data['comment_id'] . " not found"]);
        }

        $deleteMessage =
            [
                'deleted' => 1,
            ];

        if ($db->doUpdate('comments', $commentId, $deleteMessage)) {
            return json_encode(['message' => 'Oops,we have a trouble']);
        }
        return json_encode(['message' => 'Comment with id ' . $this->data['comment_id'] . ' deleted']);
    }

    // получим данные из тела запроса
    private function getParam($dataList = []): array
    {
        $string = file_get_contents('php://input');

        if (mb_strlen($string) > 0) {
            $dataList = json_decode($string, true);
        }

        return $dataList;
    }
}