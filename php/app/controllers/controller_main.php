<?php

class Controller_Main extends Controller
{
    function action_index()
    {
        $main = new Model_Main();

        echo $main->getUserList();

        return true;
    }

    function action_addUser()
    {
        $main = new Model_Main();

        echo $main->addUser();

        return true;
    }

    function action_addUserFast()
    {
        $main = new Model_Main();

        echo $main->addUserFast();

        return true;
    }

    function action_getCommentList()
    {
        $main = new Model_Main();

        echo $main->getCommentList();

        return true;
    }

    function action_addComment()
    {
        $main = new Model_Main();

        echo $main->addComment();

        return true;
    }

    function action_addCommentFast()
    {
        $main = new Model_Main();

        echo $main->addCommentFast();

        return true;
    }

    function action_doCommentStatusChange()
    {
        $main = new Model_Main();

        echo $main->doCommentStatusChange();

        return true;
    }

    function action_doDeleteComments()
    {
        $main = new Model_Main();

        echo $main->doDeleteComments();

        return true;
    }
}