FROM php:8.0-apache

COPY /php /var/www/html
WORKDIR /var/www/html

RUN a2enmod rewrite
RUN docker-php-ext-install pdo pdo_mysql
RUN chown -R www-data:www-data /var/www/html

EXPOSE 80